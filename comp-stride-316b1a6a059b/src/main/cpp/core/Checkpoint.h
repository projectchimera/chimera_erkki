#ifndef CHECKPOINT_H_INCLUDED
#define CHECKPOINT_H_INCLUDED
/*
 *  This is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2017, Willem L, Kuylen E, Stijven S & Broeckhove J
 */

/**
 * @file
 * Header for the core Cluster class.
 */

#include "core/ClusterType.h"
#include "core/ContactProfile.h"
#include "core/LogMode.h"
#include "pop/Person.h"
#include "pop/PopulationBuilder.h"
#include "sim/Simulator.h"

#include <array>
#include <cstddef>
#include <vector>
#include <string>
//#include <memory>

namespace stride {

class RngHandler;
class Calendar;

/**
 * Represents a location for social contacts, an group of people.
 */
class Checkpoint
{
public:
	/// Constructor
	Checkpoint(std::size_t frequency, std::string cfile);



private:
	//Update the checkpoint file
	void new_checkpoint ();

	void 
private:

	std::string checkpoint_file; //name of the checkpoint file
};

} // end_of_namespace

#endif // include-guard
