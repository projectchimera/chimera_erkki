/*
 *  This is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2017, Willem L, Kuylen E, Stijven S & Broeckhove J
 */

/**
 * @file
 * Header for the core Cluster class.
 */

#include "Checkpoint.h"

#include "Infector.h"
#include "LogMode.h"
#include "calendar/Calendar.h"
#include "pop/Person.h"

#include <spdlog/spdlog.h>
#include <cstddef>
#include <memory>
#include <vector>
#include "H5Cpp.h"


namespace stride {

using namespace std;

    Checkpoint::Checkpoint(std::size_t frequency, std::string cfile){
        file_id = H5Fcreate ("file.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Fclose (file_id);
    }
    void Checkpoint::new_checkpoint (){

    }


} // end_of_namespace
