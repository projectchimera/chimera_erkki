/*
 * populationGen.h
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <vector>
#ifndef SRC_POPULATIONGEN_H_
#define SRC_POPULATIONGEN_H_



class PopulationGenerator{
public:
	PopulationGenerator(std::string configFile);
	void generate(std::string filenameout);

private:
	void* randGen;

};

class Person{
public:
	Person(int household, int ageIn, int work, int school, int cluster, int CommunityIDin);
	void WriteToFile(std::ofstream* filenameout);
private:
	int age;
	int ClusterID;
	int WorkID;
	int SchoolID;
	int HouseholdID;
	int CommunityID;


};

class randGen{
public:
	float getNum(float upper, float lower, int precision);
	randGen(std::string generator, double seed);

};

class alias{
public:
	alias(std::vector<float> probabilities, std::vector<unsigned int> ids);
	int generate(float randomN, float aliasRandom); //returns the alias
private:
	int size;
	std::vector< int > Alias;
	std::vector< int > Original;
	std::vector<float> Prob;
};

class geoParser{
public:
	geoParser(std::string fileName);
	void parseFile();

private:
	int nrCities;
	std::string fileName;

};


class householdParser{
public:
	householdParser(std::string filename);
	std::vector<int> getHousehold(int seed);
private:
	std::vector< std::vector<int> > Households;
};



class populationParser{

public:
	populationParser(std::string filename);
	void parseFile();
};


struct city{
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct village{
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct school{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct college{
public:
	unsigned int size;
	unsigned int maxSize;
	unsigned int cityId;
	unsigned int id;
};

struct workplace{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct community{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};





#endif /* SRC_POPULATIONGEN_H_ */
