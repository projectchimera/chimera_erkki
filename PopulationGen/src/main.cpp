/*
 * main.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include "populationGen.h"



int main(int argc, char* argv[]){
	/*
	 * Here be testing/coding values, change later to read from file
	 * */
	/*int popSize = 100;
	int communitySize = 20;
	int nrCommunities = popSize/communitySize;
	int workSize = 5;
	int nrWorkspaces = popSize/workSize;*/
	int totalpop = 0;
/*fractie huishoudgrootte: 1 persoon [12/100], 2 personen [27/100], 3 personen
[20/100], 4 personen [22/100], 5 personen [10/100], zes of meer dan
zes personen [9/100]*/
	srand(time(NULL));

	/*
	 * Here be code
	 * */

	//float comBracket = 10/nrCommunities;
	/*for(int i = 0; i < 10; i++){	//per person
		float testcom = rand() % 10;
		int givenCom = testcom/comBracket;
		std::cout << testcom <<  " testdivision: " << givenCom << std::endl;


	}*/
	//Make a number schools/workplaces, relative to popsize

	/*

	 *vanuit GeoVerdelingsProfiel
	*/
	int nrVillages;
	float maxLatitude = 100;
	float minLatitude = 0;
	float maxLongitude = 100;
	float minLongitude = 0;

	/*
	 *
	*/
	std::vector<community> communities;
	std::vector<school> schools;
	std::vector<college> colleges;
	std::vector<workplace> workplaces;
	std::vector<city> cities;
	std::vector<village> villages;
	int popSize = 100;		//turn into readfromfile

	for(int i = 0; i < nrVillages; i++){

		//assign ID, latitude, longitude and size
		float VilLatitude = rand() + int(minLatitude) % int(maxLatitude);
		float VilLongitude = rand() + int(minLongitude) % int(maxLongitude);

		village TempVil;
		TempVil.id = i;
		TempVil.latitude = VilLatitude;
		TempVil.longitude = VilLongitude;
		TempVil.size = 0;

		villages.push_back(TempVil);
		//push into vector
	}
	int communitySize = 2000;
	int nrCommunities = popSize/communitySize;
	for(int i = 0; i < nrCommunities; i++){

		float ComLatitude = rand() + int(minLatitude) % int(maxLatitude);
		float ComLongitude = rand() + int(minLongitude) % int(maxLongitude);

		community TempCom;
		// = community{latitude = ComLatitude, longitude = ComLongitude, size = 0,id = i};
		TempCom.id = i;
		TempCom.latitude = ComLatitude;
		TempCom.longitude = ComLongitude;
		TempCom.size = 0;
		communities.push_back(TempCom);

		//assign ID, latitude, longitude and size
		//push into vector
	}
	int schoolSize = 500;
	int nrSchools = (popSize/5)/schoolSize;
	for(int i = 0; i < nrSchools; i++){
		//assign ID, latitude, longitude and size
		//push into vector

		float SchoolLatitude = rand() + int(minLatitude) % int(maxLatitude);
		float SchoolLongitude = rand() + int(minLongitude) % int(maxLongitude);

		school TempSchool;
		TempSchool.id = i;
		TempSchool.latitude = SchoolLatitude;
		TempSchool.longitude = SchoolLongitude;
		TempSchool.size = 0;
		schools.push_back(TempSchool);

	}


	int collegeSize = 3000;
	int nrColleges = (popSize/20)/collegeSize;
	std::vector<float> probability;
	std::vector<unsigned int> ids;
	int totalcitypop = 0;
	for(int k = 0; k < cities.size(); k++){
		totalcitypop += cities[k].size;
		ids.push_back(cities[k].id);
	}
	for(int k = 0; k < cities.size(); k++){
		totalcitypop += cities[k].size;
		probability.push_back(cities[k].size/totalcitypop);
	}
	alias Aliasmethod = alias(probability, ids);
	int chosenCityID;
	for(int i = 0; i < nrColleges; i++){
		chosenCityID = Aliasmethod.generate(rand(), rand());
		college tempCollege;
		tempCollege.cityId = chosenCityID;
		tempCollege.size = 0;
		tempCollege.id = i;
		tempCollege.maxSize = 3000;
		colleges.push_back(tempCollege);



		//assign ID, latitude, longitude and size
		//push into vector
	}
	int workplaceSize = 20;
	int nrWorkplaces = (popSize- popSize/3)/workplaceSize;
	for(int i = 0; i < nrWorkplaces; i++){

		float WorkLatitude = rand() + int(minLatitude) % int(maxLatitude);
		float WorkLongitude = rand() + int(minLongitude) % int(maxLongitude);

		workplace TempWork;
		TempWork.id = i;
		TempWork.latitude = WorkLatitude;
		TempWork.longitude = WorkLongitude;
		TempWork.size = 0;
		workplaces.push_back(TempWork);
		//assign ID, latitude, longitude and size
		//push into vector
	}

	//distribute the workspaces/schools evenly in the geograhic location

	std::ofstream householdFile;
	householdFile.open ("SynthPop_geo.csv");
	householdFile << "\"hh_id\",\"latitude\",\"longitude\",\"size\"\n";

	std::ofstream houseHoldFile;
	houseHoldFile.open ("SynthPop_geo.csv");
	houseHoldFile << "\"hh_id\",\"latitude\",\"longitude\",\"size\"\n";

	std::ofstream popFile;
	popFile.open ("SynthPop.csv");
	popFile <<	"\"age\",\"household_id\",\"school_id\",\"work_id\",\"primary_community\",\"secondary_community\"\n";


	householdParser households = householdParser("Households_SynthPop.csv");



	int HouseID = 0;

	//make households until you're above the asked popsize
	while(totalpop < popSize){
		//draw a size for the household
		int houseSize;
		HouseID ++;
		int houseSizeDraw = rand() % 100;
		std::vector<int> household = households.getHousehold(houseSizeDraw);
		houseSize = household.size();

		//assign cluster/location to house, and determine available schools/workplaces in area
		//assign person age, in accordance to some rules
		//per person, draw a work/school from ones available to household.
		int householdID;
		int age;
		int work;
		int school;
		int cluster;
		int community;
		std::vector<Person> PeopleInHouse;
		for(int i = 0; i < houseSize; i++){
			int workID;
			int schoolID;
			int clusterID;
			int communityID;

			int householdID = HouseID;
			int age = household[i];
			int work = workID;
			int school = schoolID;
			int cluster = clusterID;
			int community = communityID;

		}



			PeopleInHouse.push_back(Person(householdID, age, work, school, cluster, community ));
	}
		//writeToFile



	//std::cout << "Hello world!" << std::endl;
	popFile.close();
	houseHoldFile.close();
	return 0;
}


