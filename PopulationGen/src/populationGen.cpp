/*
 * populationGen.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */

#include "populationGen.h"
#include <list>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>


Person::Person(int household, int ageIn, int work, int school, int cluster, int CommunityIDin ){
	this->age = ageIn;
	this->ClusterID = cluster;
	this->WorkID = work;
	this->SchoolID = school;
	this->HouseholdID = household;
	this->CommunityID = CommunityIDin;
};


void Person::WriteToFile(std::ofstream* filenameout){

	std::stringstream converter;
	converter << this->age << "," << this->HouseholdID << "," << this->SchoolID << "," << this->WorkID
	 << "," << this->CommunityID << "," << this->CommunityID << "\n";
	std::string printstring;
	printstring = converter.str();
	//filenameout << "ladieda";
	*filenameout << printstring;

};

alias::alias(std::vector<float> probabilities, std::vector<unsigned int> ids){
	std::vector<float> T;
	std::vector<float>::iterator it = T.begin();
	//std::vector<float> Prob;
	//std::vector<std::string> Alias;
	//std::vector<std::string> Original;
	std::vector<int> AliasTemp;
	std::vector<int>::iterator it2 = AliasTemp.begin();
	int n = probabilities.size();
	size = n;
	std::vector<float>::iterator tempit = it;
	std::vector<int>::iterator tempit2 = it2;
	for(int j = 0; j < n; j++){
		//T.push_back(n*probabilities[i]);
		for(int i = 0; i < T.size(); i++){
			tempit++;
			if(T[i] < probabilities[i]){
				T.insert(tempit, probabilities[i]);
				AliasTemp.insert(tempit2,ids[i]);
				break;
			}
		}
	}

	for(int j = 1; j < n-1; j++){
		float pl = T.front();
		T.erase(it);
		float pg = T.back();
		T.pop_back();
		int g = AliasTemp.back();
		AliasTemp.pop_back();
		int tag =AliasTemp.front();
		AliasTemp.erase(it2);
		Prob.push_back(pl);
		Original.push_back(tag);
		Alias.push_back(g);
		pg = pg - (1 - pl);
		tempit = it;
		tempit2 = it2;
		if(pg != 0){
			for(int i = 0; i < T.size(); i++){
				tempit++;
				tempit2++;
				if(T[i] < pg){
					T.insert(tempit, pg);
					AliasTemp.insert(tempit2, g);
					break;
				}
			}
		}
	}

};

int alias::generate(float randomN, float aliasRandom){

	int n = randomN * size;
	int aliasID = Original[n];
	if(aliasRandom >= Prob[n]){
		aliasID = Alias[n];
	}
	return aliasID;
};


householdParser::householdParser(std::string filename){
	std::ifstream infile(filename);
	while (infile)
	{
		std::string s;
		if (!getline( infile, s )) break;

			std::istringstream ss( s );
			std::vector <int> record;

		while (ss)
		{
			std::string s;
			if (!getline( ss, s, ',' )) break;
			if(s == "NA" || s == "hh_age1") break;
			const char *convert = s.c_str();

			record.push_back( atoi(convert) );
		}

		Households.push_back( record );
	}
};


std::vector<int> householdParser::getHousehold(int seed){
	int HouseholdNumber = seed % Households.size();
	return Households[HouseholdNumber];
};

/*
 * ifstream infile;
int array[20];
int i=0;
char cNum[10] ;
                infile.open ("test.txt", ifstream::in);
                if (infile.is_open())
                {
                        while (infile.good())
                        {
                                infile.getline(cNum, 256, ',');
                                array[i]= atoi(cNum) ;
                                i++ ;
                        }
                        infile.close();
                }
                else
                {
                        cout << "Error opening file";
                }
 * */



