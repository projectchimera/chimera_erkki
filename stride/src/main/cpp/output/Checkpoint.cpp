
#include "Checkpoint.h"
#include "util/InstallDirs.h"


bool write_array_rank1(hid_t loc_id, std::string name, void const* data, hid_t type_id, hsize_t dim_1) {
    //  'name' may not already exists in loc_id
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        // Create rank 1 dataspace
        hid_t dspace = H5Screate_simple(1, &dim_1, 0);
        // Create dataset
        hid_t dset = H5Dcreate(loc_id, name.c_str(), type_id, dspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        // Write data using the native datatype corresponding to the provided type_id
        hid_t mem_type_id = H5Tget_native_type(type_id, H5T_DIR_ASCEND);
        H5Dwrite(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
        // Clean up HDF5 handles
        H5Tclose(mem_type_id);
        H5Dclose(dset);
        H5Sclose(dspace);
        return true;
    } else {
        return false;
    }
}

bool write_array_rank1(hid_t loc_id, std::string name, const std::vector<std::string>& data) {
  //  'name' may not already exists in loc_id
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        // Use C-style string datatype
        hid_t dtype = H5Tcopy(H5T_C_S1);
        H5Tset_size(dtype, H5T_VARIABLE);
        // Create rank 1 dataspace
        hsize_t dim_1 = data.size();
        hid_t dspace = H5Screate_simple(1, &dim_1, 0);
        // Create dataset
        hid_t dset = H5Dcreate(loc_id, name.c_str(), dtype, dspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        // make array of raw strings
        std::vector<const char*> raw_string_ptrs;
        for (const std::string & s : data) raw_string_ptrs.push_back(s.c_str());
        H5Dwrite(dset, dtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, raw_string_ptrs.data());
        // Clean up HDF5 handles
        H5Dclose(dset);
        H5Sclose(dspace);
        H5Tclose(dtype);
        return true;
    } else {
        return false;
    }
}

bool write_array_rank2(hid_t loc_id, std::string name, void const* data, hid_t type_id, hsize_t dim_1, hsize_t dim_2) {
  //  'name' may not already exists in loc_id
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        // Create rank 2 dataspace
        hsize_t dims[2] = {dim_1, dim_2};
        hid_t dspace = H5Screate_simple(2, dims, 0);
        // Create dataset
        hid_t dset = H5Dcreate(loc_id, name.c_str(), type_id, dspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        // Write data
        hid_t mem_type_id = H5Tget_native_type(type_id, H5T_DIR_ASCEND);
        H5Dwrite(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
        // Clean up HDF5 handles
        H5Tclose(mem_type_id);
        H5Dclose(dset);
        H5Sclose(dspace);
        return true;
    } else {
        return false;
    }
}

template <typename T>
bool read_array_rank1(hid_t loc_id, std::string name, T** data, hid_t mem_type_id, size_t* dim_1) {
    // HDF5 error status. Used to check if bad things are happening.
    herr_t status;

    // check for existence 'name'
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
          return false;
    }

    // Try to open the dataset
    hid_t dset = H5Dopen(loc_id, name.c_str(), H5P_DEFAULT);
    if (dset < 0) {
            return false;
    }

    // Get the associated dataspace
    hid_t dspace = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(dspace);
    if (ndims != 1) {
      return false;
    }

    // Number of elements in data
    hsize_t _dim_1;
    H5Sget_simple_extent_dims(dspace, &_dim_1, 0);
    *dim_1 = static_cast<size_t>(_dim_1);

    // Allocate space for data
    *data = new T[_dim_1];

    // Read the data
    status = H5Dread(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, *data);
    if (status < 0) {
      return false;
    }

    // Clean up handles
    H5Sclose(dspace);
    H5Dclose(dset);
    return true;
}

template <typename T>
bool read_array_rank2(hid_t loc_id, std::string name, T** data, hid_t mem_type_id, size_t* dim_1, size_t* dim_2) {
    // HDF5 error status. Used to check if bad things are happening.
    herr_t status;

    // Quickly check for existence of 'name'
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        return false;
    }

    // Try to open the dataset
    hid_t dset = H5Dopen(loc_id, name.c_str(), H5P_DEFAULT);
    if (dset < 0) {
      return false;
    }

    // Get the associated dataspace
    hid_t dspace = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(dspace);
    if (ndims != 2) {
      return false;
    }

    // Number of elements in data
    hsize_t _dims[2];
    H5Sget_simple_extent_dims(dspace, _dims, 0);
    *dim_1 = static_cast<size_t>(_dims[0]);
    *dim_2 = static_cast<size_t>(_dims[1]);

    // Allocate space for data
    *data = new T[_dims[0] * _dims[1]];

    // Read the data
    status = H5Dread(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, *data);
    if (status < 0) {
      return false;
    }

    // Clean up handles
    H5Sclose(dspace);
    H5Dclose(dset);
    return true;
}

//herr_t (*H5L_iterate_t)( hid_t g_id, const char *name, const H5L_info_t *info, void *op_data)
herr_t collect_link_names(hid_t , const char* name, const H5L_info_t* , void* op_data) {
    std::list<std::string>* link_names = static_cast<std::list<std::string>*>(op_data);
    link_names->push_back(std::string(name));
    return 0;
}

std::list<std::string> find_matching_links(hid_t loc_id, std::function<bool(std::string)> match) {
    // Collect all link names at location loc_id
    std::list<std::string> link_names;
    H5Literate(loc_id, H5_INDEX_NAME, H5_ITER_NATIVE, 0, &collect_link_names, &link_names);
    // Remove link names that DON'T match the 'match' predicate
    link_names.remove_if([&](std::string name)->bool{ return !match(name); });
    return link_names;
}




namespace stride {
namespace output {

using namespace stride::util;

//Create empty hdf5 exporter

Checkpoint::Checkpoint(const std::string& file):file_id(-1), output_dir(""){
  output_dir = InstallDirs::GetCurrentDir().string() + std::string("_checkpoint.h5");

}

//Destructor to clean hdf5 storage
Checkpoint::~Checkpoint(){
  if (IsOpen()){
    CloseFile();
  }
}

void Checkpoint::CloseFile(){
  if (file_id >= 0) {
    H5Fclose(file_id);
    output_dir = "";
  }

}

bool Checkpoint::IsOpen(){
  return (output_dir != "") && (file_id != -1);
}

bool Checkpoint::Open(const std::string f_name){
  if (IsOpen()) {
    CloseFile();
  }
  bool new_file = false;
  bool succes = true;

  if (f_name != "") {
      // Try to open the file if it exists and is an HDF5 file
      // Save old error handler and turn off error handling temporarily since this function
      // fail and will clutter up the stdout.
      herr_t (*old_func)(hid_t, void*);
      void *old_client_data;
      H5Eget_auto(H5E_DEFAULT, &old_func, &old_client_data);
      H5Eset_auto(H5E_DEFAULT, NULL, NULL);
      htri_t is_hdf5 = H5Fis_hdf5(f_name.c_str());
      // Restore previous error handler
      H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
      if (0 < is_hdf5) {
          // File exists and is in HDF5 format, open it
          file_id = H5Fopen(f_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
          if (file_id >= 0) {
              new_file = false;
              output_dir = f_name;
              succes = true;
          }
          else {
            succes = false;
          }
      } else if (0 == is_hdf5) {
          succes = false;
      } else {
          // File does not exist, create a new one
          file_id = H5Fcreate(f_name.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
          if (file_id < 0) {
              succes = false;
          } else {
              succes = true;
              new_file = true;
              output_dir = f_name;
          }
      }
  }
  if (succes && new_file) {
      // Create extendable integer array dataset
      // to hold the simulation steps indices (integers)
      hsize_t steps_dims = 0;                        // Initial size: no elements
      hsize_t steps_max_dims = H5S_UNLIMITED;        // Can extend with no bounds

      // Create the data space, first argument 'rank' is 1 (i.e. 1 dimensional array)
      hid_t steps_dspace = H5Screate_simple(1, &steps_dims, &steps_max_dims);

      // Create properties for dataset creation: chunking is necessary for extendable data sets
      hid_t steps_dset_props = H5Pcreate(H5P_DATASET_CREATE);
      hsize_t steps_chunk_dims = 10;                                 // Rather arbitrary chunk size
      H5Pset_chunk(steps_dset_props, 1, &steps_chunk_dims);     // 1 is the rank of a 1D array


      // Create data set '/steps_idx' (link creation and data access properties are default)
      hid_t steps_idx_dset = H5Dcreate(file_id, "/steps_idx", H5T_STD_I32LE,
              steps_dspace, H5P_DEFAULT, steps_dset_props, H5P_DEFAULT);
      if (steps_idx_dset < 0) {
          succes = false;
      }

      // Clean up
      H5Dclose(steps_idx_dset);
      H5Sclose(steps_dspace);
      H5Pclose(steps_dset_props);
  }

  return succes;


}

void Checkpoint::RestoreCheckpoint(int step_id){

  // The Sim/MeshState that will be filled up with information form HDF5 and returned
  std::vector<int> infected_persons;
  std::vector<int> start_infectiousness_vec;
  std::vector<int> start_symptomatic_vec;
  std::vector<int> end_infectiousness_vec;
  std::vector<int> end_symptomatic_vec;
  std::vector<int> persons_id;

  // =========================================================================
  // Read information about steps contained in the file
  int* time_steps_idx_data = nullptr;
  size_t time_steps_idx_dim1 = 0;
  read_array_rank1(file_id, "time_steps_idx", &time_steps_idx_data, H5T_NATIVE_INT, &time_steps_idx_dim1);

  if (-1 == step_id) {
      // Use the last time step if no step was requested
      step_id = time_steps_idx_dim1 - 1;
  }

  const size_t sim_step = time_steps_idx_data[step_id];

  // Try to open the requested time step
  std::string step_grp_name = std::string("/step_") + std::to_string(sim_step);
  hid_t step_grp_id = H5Gopen(file_id, step_grp_name.c_str(), H5P_DEFAULT);



  // Clean up temp storage
  delete[] time_steps_idx_data;

  // =========================================================================

  RestorPopulation(step_grp_id,  infected_persons, start_infectiousness_vec, start_symptomatic_vec, end_infectiousness_vec,
                          end_symptomatic_vec, persons_id);
}


void Checkpoint::RestorPopulation(hid_t loc_id,  std::vector<int>& infected_persons, std::vector<int>& start_infectiousness_vec,
                        std::vector<int>& start_symptomatic_vec, std::vector<int>& end_infectiousness_vec,
                        std::vector<int>& end_symptomatic_vec, std::vector<int>& persons_id){
                          // Reading nodes_id
         int* persons_id_data = nullptr;
         size_t persons_id_dim1 = 0;
         read_array_rank1(loc_id, "nodes_id", &persons_id_data, H5T_NATIVE_INT, &persons_id_dim1);
         // Will be used as reference size to validate other sizes
         const size_t& num_persons = persons_id_dim1;

         // Reading person_health
         double* person_health_data = nullptr;
         size_t person_health_dim1 = 0;
         size_t person_health_dim2 = 0;
         read_array_rank2(loc_id, "health_info", &person_health_data, H5T_NATIVE_DOUBLE, &person_health_dim1, &person_health_dim2);

         // Verify dimensions
         if (num_persons != person_health_dim1) {
           std::cout << "Something went wrong" << std::endl;
         }

         // Build nodes from data read before
         for (size_t person_idx = 0; person_idx < num_persons; ++person_idx) {
           persons_id.push_back(persons_id_data[person_idx]);
           start_infectiousness_vec.push_back(person_health_data[5*person_idx]);
           start_symptomatic_vec.push_back(person_health_data[5*person_idx + 1]);
           end_infectiousness_vec.push_back(person_health_data[5*person_idx + 2]);
           end_symptomatic_vec.push_back(person_health_data[5*person_idx + 3]);
           infected_persons.push_back(person_health_data[5*person_idx + 4]);
         }

         // Clean up temp storage
         delete[] persons_id_data;
         delete[] person_health_data;


  }


void Checkpoint::MakeCheckpoint(std::shared_ptr<const stride::Simulator> simulator, unsigned int step){

	// ========================================================================
	// First do the following checks
	// - Can '/steps_idx' be opened & are the dimensions correct?
	//  -> fatal if condition not met
	// - Is step already present in 'steps_idx' OR are there any steps beyond?
	//  -> remove all time steps(_idx) beyond that point, then add new time step
	// - Is the group '/step_n' already present for the current step?
	//  -> remove all /step_n groups starting from that point, then add new time step group
	// ========================================================================

	int* steps_idx_data = nullptr;
	size_t steps_idx_dim1 = 0;
	read_array_rank1(file_id, "steps_idx", &steps_idx_data, H5T_NATIVE_INT, &steps_idx_dim1);



	// Shorthand notations
	size_t& num_time_steps = steps_idx_dim1;
	const int sim_step = step;
	std::string step_grp_name = std::string("/step_") + std::to_string(sim_step);


	// Safe cases where no truncation is done are:
	//   num_time_steps = 0
	//   sim_step > steps_idx[end]
	// Hence truncation must be performed in the following case:
	if (num_time_steps != 0 && sim_step <= steps_idx_data[num_time_steps - 1]) {
		// Logger::get()->info() << "[Hdf5File] File will be truncated.";
		// To find the last step in steps_idx BEFORE sim_step loop over
		// the steps_idx values backwards until sim_step is reached
		size_t i = num_time_steps - 1;
		while (static_cast<int>(i) >= 0 && sim_step <= steps_idx_data[i]) {
			std::string name = std::string("step_") + std::to_string(steps_idx_data[i]);
			H5Ldelete(file_id, name.c_str(), H5P_DEFAULT);
			--i;
		}

			hid_t dset = H5Dopen(file_id, "steps_idx", H5P_DEFAULT);
			hsize_t dims;
			hid_t dspace = H5Dget_space(dset);
			H5Sget_simple_extent_dims(dspace, &dims, 0);
			dims = i + 1;
			H5Dset_extent(dset, &dims);
			H5Sclose(dspace);
			H5Dclose(dset);

	}

	// Clean up temporary structures for time_steps
	delete[] steps_idx_data;


	// Extending /steps_idx with current step index ===================
	hid_t steps_idx_dset = H5Dopen(file_id, "steps_idx", H5P_DEFAULT);

	// Get the current size of /steps_idx, increase the size by one
	// and extend the /steps_idx data set to accommodate the new time step value
	hsize_t steps_idx_dims;
	hid_t steps_idx_dspace = H5Dget_space(steps_idx_dset);
	H5Sget_simple_extent_dims(steps_idx_dspace, &steps_idx_dims, 0);
	steps_idx_dims++;
	H5Dset_extent(steps_idx_dset, &steps_idx_dims);

	// To append the current step index to /steps_idx properly:
	//  1. create the destination data space to append the current time step index
	//     to the newly extended /steps_idx
	//     (i.e. select the last point of /steps_idx)
	//          H5S_SELECT_SET overwrites any previous selection
	//          1 is the number of elements that are selected
	//          last_time_step_coord is the index of the last /steps_idx element after expansion
	//  2. create the source data space for the current step index
	//     (i.e. just a simple data space curr_time_step_idx_dspace for one element)
	//  3. Write the current step index value with the previously defined source/destination spaces

	// 1.
	hsize_t last_time_step_idx_coord = steps_idx_dims - 1;
	H5Sselect_elements(steps_idx_dspace, H5S_SELECT_SET, 1, &last_time_step_idx_coord);
	// 2.
	hsize_t curr_time_step_idx_dims[] = { 1 };
	hid_t curr_time_step_idx_dspace = H5Screate_simple(1, curr_time_step_idx_dims, NULL);
	// 3.
	H5Dwrite(steps_idx_dset, H5T_STD_I32LE, curr_time_step_idx_dspace, steps_idx_dspace, H5P_DEFAULT,
	        &sim_step);
	// Clean up HDF5 handles
	H5Sclose(curr_time_step_idx_dspace);
	H5Sclose(steps_idx_dspace);
	H5Dclose(steps_idx_dset);
	// End of extending /steps_idx ====================================

	// Creating & writing the '/step_n' group ==============================
	hid_t step_grp = H5Gcreate(file_id, step_grp_name.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	// Save the mesh: Nodes, Cells & Walls to '/step_n' group
	Save_Population(step_grp, simulator->GetPopulation());

	H5Gclose(step_grp);
	// End of creating & writing the '/step_n' group =======================
}

void Checkpoint::Save_Population(hid_t loc_id, const std::shared_ptr<const Population> m_population){
  const size_t num_persons = m_population->size();
  std::vector<int> infected_persons;
  std::vector<int> start_infectiousness_vec;
  std::vector<int> start_symptomatic_vec;
  std::vector<int> end_infectiousness_vec;
  std::vector<int> end_symptomatic_vec;
  std::vector<int> persons_id;
  for (unsigned int i = 0; i < num_persons;i++){
    persons_id.push_back(m_population->at(i).GetId());
    start_infectiousness_vec.push_back(m_population->at(i).GetHealth().GetStartInfectiousness());
    start_symptomatic_vec.push_back(m_population->at(i).GetHealth().GetStartSymptomatic());
    end_infectiousness_vec.push_back(m_population->at(i).GetHealth().GetEndInfectiousness());
    end_symptomatic_vec.push_back(m_population->at(i).GetHealth().GetEndSymptomatic());
    if (m_population->at(i).GetHealth().IsInfected()){
      infected_persons.push_back(1);
    }
    else {
      infected_persons.push_back(0);
    }
  }
  write_array_rank1(loc_id, "health_id", persons_id.data() , H5T_STD_I32LE, num_persons);
  int* persons_data = new int[5*num_persons];
  for (size_t i = 0; i < num_persons; ++i) {
    persons_data[5*i] = start_infectiousness_vec.at(i);
    persons_data[5*i + 1] = start_symptomatic_vec.at(i);
    persons_data[5*i + 2] = end_infectiousness_vec.at(i);
    persons_data[5*i + 3] = end_symptomatic_vec.at(i);
    persons_data[5*i + 4] = infected_persons.at(i);
  }
  write_array_rank2(loc_id, "health_info", persons_data, H5T_STD_I32LE, num_persons, 5);
}





} // end_of_namespace
} // end_of_namespace
