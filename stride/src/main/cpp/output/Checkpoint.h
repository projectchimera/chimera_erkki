#ifndef CHECKPOINT_H_INCLUDED
#define CHECKPOINT_H_INCLUDED



#include "sim/Simulator.h"
#include "sim/SimulatorBuilder.h"
#include "pop/Person.h"
#include "pop/Population.h"

#include <list>
#include <string>
#include <vector>
#include <hdf5.h>


namespace stride {
  class Simulator;

}

namespace stride {
namespace output {
  class Checkpoint {
    public:
      Checkpoint(const std::string& file = "stride_checkpoints.h5");
      //Destructor to clean hdf5 storage
      virtual ~Checkpoint();

      void CloseFile();

      bool IsOpen();

      bool Open(const std::string f_name);

      void RestoreCheckpoint(int step_id);

      void MakeCheckpoint(std::shared_ptr<const stride::Simulator> simulator, unsigned int step);

    private:
      hid_t file_id;
      std::string output_dir;
    private:
      void Save_Population(hid_t loc_id, const std::shared_ptr<const Population>);
      void RestorPopulation(hid_t loc_id,  std::vector<int>& infected_persons, std::vector<int>& start_infectiousness_vec,
                              std::vector<int>& start_symptomatic_vec, std::vector<int>& end_infectiousness_vec,
                              std::vector<int>& end_symptomatic_vec, std::vector<int>& persons_id);


  };
} // end_of_namespace
} // end_of_namespace


#endif // end of include guard
